import React from 'react';

class AddRecipe extends React.Component {

  handleSubmit(e) {
    e.preventDefault();
    this.props.cb(this.name.value);
    this.name.value = '';
  }

  render() {
    return (
      <form onSubmit={ this.handleSubmit.bind(this) }>
        <input type="text" ref={ elem => this.name = elem  } />
        <button>Add</button>
      </form>
    )
  }
}

export default AddRecipe;