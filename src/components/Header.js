import React from 'react';
import PropTypes from 'prop-types';

const Header = ({ msg, children, cb }) => (
  <h1 onClick={ () => cb() }>{ msg } : { children }</h1>
);


Header.propTypes = {
};

export default Header;