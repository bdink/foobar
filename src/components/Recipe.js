import React from 'react';

const Recipe = ({ recipe }) => (
  <li>{ recipe }</li>
);

export default Recipe;