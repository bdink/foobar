import React from 'react';
import Recipe from './Recipe';
import PropTypes from 'prop-types';

const Recipes = ({ recipes }) => (
  <ul>
    {
      recipes.map(recipe => <Recipe key={ recipe } recipe={ recipe }/>)
    }
  </ul>
);

Recipes.propTypes = {
  recipes: PropTypes.array.isRequired
};

export default Recipes;