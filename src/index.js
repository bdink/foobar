import React from 'react';
import ReactDOM from 'react-dom';
import AddRecipe from 'components/AddRecipe';
import Recipes from 'components/Recipes';
import Header from 'components/Header';

let recipes = ['Eggs', 'Pancake', 'Shuarma'];

class App extends React.Component {
  constructor() {
    super();

    this.state = {
      recipes: [1,2,3]
    }
  }

  addRecipe(name) {
    this.state.recipes.push(name);
    this.forceUpdate();
  }

  render() {
    return (
      <div>
        <Header msg="Hi everyone" cb={ () => console.log('Foo') }>WTF?</Header>
        <Recipes recipes={ this.state.recipes }/>
        <AddRecipe cb={ this.addRecipe.bind(this) } />
      </div>
    );
  }
}

ReactDOM.render(
  <div>
    <App />
    <App />
    <App />
  </div>,
  document.getElementById('root')
);
